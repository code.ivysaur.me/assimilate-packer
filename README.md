# assimilate-packer

![](https://img.shields.io/badge/written%20in-bash-blue)

A shell script to handle precomp+zpaq compression workflow.

Zip archives of nearly-identical data might be almost entirely different after compression. Best compression can be had by unpacking the archives first; but naively doing so loses bit-exact archive reconstruction. By using a `precomp` pass, as well as a global deduplicating compressor, maximum storage efficiency can be achieved.

- Parallel precomp scanning
- Graphical extraction interface
- Dependencies: `precomp`, `zpaq`, `dialog`, and GNU Parallel
- Assumes Cygwin, although porting would be possible

## Usage


```
Usage:
  assimilate [options]

Options:
  {no option argument}  Begin interactive pack/unpack workflow
  --child 'arg'         (Internal use only)
  --help                Display this message
  --pack                Immediately pack
  --unpack-menu         Begin interactive unpack workflow
```


## Changelog

2015-11-08: r04
- Fix an issue packing and unpacking un-`precomp`-able files
- [⬇️ assimilate-packer-r04.tar.xz](dist-archive/assimilate-packer-r04.tar.xz) *(1.69 KiB)*


2015-11-08: r01
- Inital public release
- [⬇️ assimilate-packer-r01.tar.xz](dist-archive/assimilate-packer-r01.tar.xz) *(1.64 KiB)*

